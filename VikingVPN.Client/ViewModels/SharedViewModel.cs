﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Reactive.Concurrency;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using VikingVPN.Client.Models;
using VikingVPN.Client.Views;
using Status = VikingVPN.Client.Models.VpnConnection.ConnectionStatus;
using System.Net;
using VikingVPN.Client.Helpers;
using System.Diagnostics;

namespace VikingVPN.Client.ViewModels
{
    public class SharedViewModel
    {
        public ReactiveProperty<string> Icon { get; private set; }

        public ReactiveProperty<bool> HasConfigurationFiles { get; private set; }

        public ReactiveProperty<bool> IsConnected { get; private set; }
        public ReactiveProperty<bool> IsConnecting { get; private set; }
        public ReactiveProperty<bool> IsNotConnected { get; private set; }
        public ReactiveProperty<Status> ConnectionStatus { get; private set; }
        public ReactiveProperty<Status> ConnectionStatusWithIp { get; private set; }
        public ReactiveProperty<bool> IsActiveConnectionStatus { get; private set; }
        public ReactiveProperty<bool> IpAddressIsDifferentThanOriginal { get; private set; }
 
        public ReactiveProperty<IPAddress> CurrentIPAddress { get; private set; }
        public ReactiveProperty<IPAddress> OriginalIPAddress { get; private set; }

        public SharedViewModel(AppContext app)
        {

            this.OriginalIPAddress = app.Master.ObserveProperty(m => m.OriginalIp).Where(ip => ip != null).ToReactiveProperty();

            this.HasConfigurationFiles = app.Master.ObserveProperty(m => m.Configurations).Select(c => c.Any()).ToReactiveProperty();

            this.ConnectionStatus = app.Master
                                        .ObserveProperty(m => m.Connection)
                                        .SelectMany(c => c != null ? c.ObserveProperty(x => x.Status) : new ReactiveProperty<Status>(Status.Disconnected))
                                        .ObserveOn(ThreadPoolScheduler.Instance)
                                        .ToReactiveProperty();


            this.CurrentIPAddress = this.ConnectionStatus.Where(cs => cs == Status.Connected)
                                        .Zip(Observable.Interval(TimeSpan.FromSeconds(10)).StartWith(1), (s, i) => s)
                                        .Where(cs => cs == Status.Connected)
                                        .Select(_ => WebRequestHelper.GetCurrentIpAddress())
                                        .ObserveOn(ThreadPoolScheduler.Instance)
                                        .ToReactiveProperty();
                                        

            var activeConnectionStatuses = new[] { VpnConnection.ConnectionStatus.Connected, VpnConnection.ConnectionStatus.Connecting, VpnConnection.ConnectionStatus.Error };
            this.IsActiveConnectionStatus = this.ConnectionStatus.Select(x => activeConnectionStatuses.Contains(x)).ToReactiveProperty();


            var ipAddressesAreTheSame = this.OriginalIPAddress.CombineLatest(this.CurrentIPAddress, (o, c) => object.Equals(o, c));

            this.IpAddressIsDifferentThanOriginal = ipAddressesAreTheSame.Select(x => !x).ToReactiveProperty();

            this.IsConnecting = this.ConnectionStatus.Select(x => x == Status.Connecting).ToReactiveProperty();
            this.IsNotConnected = this.ConnectionStatus.Select(x => x != Status.Connected).ToReactiveProperty();
            this.IsConnected = this.ConnectionStatus.Select(x => x == Status.Connected).ToReactiveProperty();

            this.ConnectionStatusWithIp = this.ConnectionStatus.CombineLatest(ipAddressesAreTheSame, (status, areTheSame) =>
            {
                if (status == Status.Connected && areTheSame)
                    return Status.Error;

                return status;
            }).ToReactiveProperty();


            this.Icon = this.ConnectionStatusWithIp.Select(x =>
            {
                if (x == Status.Connected)
                    return "/viking_green.ico";

                if (x == Status.Error)
                {
                    return "/viking_red.ico";
                }

                return "/viking_blue.ico";

            }).ToReactiveProperty();
        }
    }
}
