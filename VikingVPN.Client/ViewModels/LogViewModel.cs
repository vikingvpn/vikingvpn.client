﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Models;
using System.Net;
using System.Windows.Media;

namespace VikingVPN.Client.ViewModels
{
    public class LogViewModel : ViewModelBase
    {
        public ReadOnlyReactiveCollection<string> ConnectionLog { get; private set; }

        public ReactiveProperty<bool> IsActiveConnectionStatus { get; private set; }

        public ReactiveProperty<string> OriginalIPAddress { get; private set; }
        public ReactiveProperty<string> CurrentIPAddress { get; private set; }

        public ReactiveProperty<SolidColorBrush> CurrentIPBrush { get; private set; }

        public ReactiveCommand DisconnectCommand { get; private set; }

        public LogViewModel(AppContext app, SharedViewModel shared)
        {
            this.IsActiveConnectionStatus = shared.IsActiveConnectionStatus;
            this.OriginalIPAddress = shared.OriginalIPAddress.Select(x => x != null ? x.ToString() : "").ToReactiveProperty();
            this.CurrentIPAddress = shared.CurrentIPAddress.Select(x => x != null ? x.ToString() : "").ToReactiveProperty();
            this.ConnectionLog = app.Master.ObserveProperty(m => m.Connection).Where(c => c != null).SelectMany(c => c.Output).ToReadOnlyReactiveCollection();

            this.CurrentIPBrush = shared.IpAddressIsDifferentThanOriginal.Select(x => x ? Brushes.Green : Brushes.Red).ToReactiveProperty();

            this.DisconnectCommand = shared.IsActiveConnectionStatus.ToReactiveCommand();
            this.DisconnectCommand.Subscribe(_ =>
            {
                app.Master.Disconnect();
            });
        }
    }
}
