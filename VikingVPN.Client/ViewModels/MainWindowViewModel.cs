﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using VikingVPN.Client.Models;
using VikingVPN.Client.Services;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace VikingVPN.Client.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public ReactiveProperty<string> Icon { get; private set; }

        public ReactiveCollection<ServerViewModel> Servers { get; private set; }

        public ReactiveProperty<bool> HasConfigurationFiles { get; private set; }

        public ReactiveProperty<bool> IsNotConnected { get; private set; }

        public ReactiveProperty<bool> ShouldShowServerMenu { get; private set; }
        public ReactiveProperty<bool> ShouldShowSignIn { get; private set; }
        public ReactiveProperty<bool> ShouldShowActiveConnection { get; private set; }

        public LogViewModel LogView { get; private set; }
        public SignInViewModel SignInView { get; private set; }

        public MainWindowViewModel(AppContext app, SharedViewModel shared)
        {
            this.LogView = new LogViewModel(app, shared);
            this.SignInView = new SignInViewModel(app, shared);

            this.Icon = shared.Icon;
            this.HasConfigurationFiles = shared.HasConfigurationFiles;
            this.IsNotConnected = shared.IsNotConnected;

            this.Servers = app.Master.ObserveProperty(m => m.Configurations)
                .SelectMany(c => c)
                .Select(c => new ServerViewModel(app, c))
                .ToReactiveCollection();

            this.ShouldShowSignIn = this.HasConfigurationFiles.Select(x => !x).ToReactiveProperty();

            this.ShouldShowActiveConnection = shared.IsActiveConnectionStatus;
            this.ShouldShowServerMenu = this.ShouldShowSignIn.CombineLatest(this.ShouldShowActiveConnection, (a, b) => !a && !b).ToReactiveProperty();
        }
    }
}
