﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Models;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Diagnostics;

namespace VikingVPN.Client.ViewModels
{
    public class SignInViewModel
    {
        public ReactiveProperty<bool> HasConfigurationFiles { get; private set; }
        public ReactiveCollection<string> Errors { get; private set; }

        public ReactiveProperty<string> Email { get; private set; }

        public ReactiveCommand SignIn { get; private set; }
        public ReactiveCommand SignUp { get; private set; }

        public SignInViewModel(AppContext app, SharedViewModel shared)
        {
            this.HasConfigurationFiles = shared.HasConfigurationFiles;
            this.Email = new ReactiveProperty<string>("", ReactivePropertyMode.DistinctUntilChanged | ReactivePropertyMode.RaiseLatestValueOnSubscribe);
            this.Errors = new ReactiveCollection<string>();

            this.SignIn = new ReactiveCommand();
            this.SignIn.Subscribe(args =>
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    var pass = ((PasswordBox)args).Password;
                    var email = this.Email.Value;

                    this.Errors.Clear();

                    try
                    {
                        app.Master.EnsureTapAdapterInstalled();
                        app.Master.InstallConfigurationFiles(email, pass);
                        app.Master.Load();
                    }
                    catch (ValidationException ex)
                    {
                        foreach (var err in ex.Errors)
                        {
                            this.Errors.Add(err);
                        }
                    }
                });
            });

            this.SignUp = new ReactiveCommand();
            this.SignUp.Subscribe(args =>
            {
                Process.Start(new ProcessStartInfo("https://vikingvpn.com/pricing"));
            });
        }
    }
}
