﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Reactive.Concurrency;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using VikingVPN.Client.Models;
using VikingVPN.Client.Views;
using Microsoft.Practices.ServiceLocation;

namespace VikingVPN.Client.ViewModels
{
    /// <summary>
    /// Provides bindable properties and commands for the NotifyIcon. In this sample, the
    /// view model is assigned to the NotifyIcon in XAML. Alternatively, the startup routing
    /// in App.xaml.cs could have created this view model, and assigned it to the NotifyIcon.
    /// </summary>
    public class NotifyIconViewModel : ViewModelBase
    {
        public ReactiveProperty<string> Icon { get; private set; }
        public ReactiveProperty<bool> WindowDoesntExist { get; private set; }
        public ReactiveProperty<bool> WindowDoesExist { get; private set; }

        public ReactiveCommand ShowWindow { get; private set; }

        public ReactiveCommand HideWindow { get; private set; }

        public ReactiveCommand ExitApplication { get; private set; }

        public ReactiveCommand ShowConnectionErrorTip { get; private set; }

        public ReactiveProperty<bool> HasNoConfigurationFiles { get; private set; }

        public ReactiveProperty<bool> IsNotConnected { get; private set; }

        public ReactiveProperty<bool> ShouldShowMainWindow { get; private set; }

        public NotifyIconViewModel(AppContext app, SharedViewModel shared)
        {
            this.Icon = shared.Icon;

            this.HasNoConfigurationFiles = shared.HasConfigurationFiles.Select(x => !x).ToReactiveProperty();
            this.IsNotConnected = shared.IsNotConnected;

            this.WindowDoesntExist = new ReactiveProperty<bool>(true);
            this.WindowDoesExist = this.WindowDoesntExist.Select(w => !w).ToReactiveProperty();

            this.ShowWindow = this.WindowDoesntExist.ToReactiveCommand();
            this.ShowWindow.Subscribe(_ =>
            {
                this.WindowDoesntExist.Value = false;
                Application.Current.MainWindow = new MainWindow();
                Application.Current.MainWindow.Closing += MainWindow_Closing;
                Application.Current.MainWindow.Show();
            });

            this.HideWindow = this.WindowDoesExist.ToReactiveCommand();
            this.HideWindow.Subscribe(_ =>
            {
                this.WindowDoesntExist.Value = true;
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if(Application.Current.MainWindow != null)
                    {
                        Application.Current.MainWindow.Close();
                    }
                }));
            });

            this.ExitApplication = new ReactiveCommand();
            this.ExitApplication.Subscribe(_ =>
            {
                Application.Current.Shutdown();
            });

            var NoConfigurationFilesOrNoConnection = this.HasNoConfigurationFiles.CombineLatest(this.IsNotConnected, (a, b) => a || b).ToReactiveProperty();

            this.ShouldShowMainWindow = this.WindowDoesntExist.CombineLatest(NoConfigurationFilesOrNoConnection, (a, b) => a && b).ToReactiveProperty();
            this.ShouldShowMainWindow.FirstAsync().Where(x => x).Subscribe(_ =>
            {
                this.ShowWindow.Execute();
            });

            shared.ConnectionStatusWithIp.Where(c => c == VpnConnection.ConnectionStatus.Connected).Subscribe(_ =>
            {
                App.TriggerConnectMessage();
                this.HideWindow.Execute();
            });

            var errorStatus = shared.ConnectionStatusWithIp.Where(c => c == VpnConnection.ConnectionStatus.Error);

            errorStatus.Subscribe(_ =>
            {
                App.TriggerConnectionErrorPopup();
                app.Master.Reconnect();
            });
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.WindowDoesntExist.Value = true;
        }
    }
}
