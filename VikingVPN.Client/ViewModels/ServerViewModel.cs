﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Models;

namespace VikingVPN.Client.ViewModels
{
    public class ServerViewModel : ViewModelBase
    {
        public ReactiveProperty<string> Name { get; private set; }
        public ReactiveCommand ConnectCommand { get; private set; }
        public ReactiveProperty<bool> IsNotConnected { get; private set; }

        public ServerViewModel(AppContext app, ConfigurationFile configFile)
        {
            this.Name = configFile.ObserveProperty(x => x.Name).ToReactiveProperty();
            this.IsNotConnected = app.Master.ObserveProperty(x => x.Connection).Select(c => c == null).ToReactiveProperty();

            this.ConnectCommand = new Reactive.Bindings.ReactiveCommand();
            this.ConnectCommand.Subscribe(_ =>
            {
                app.Master.Connect(configFile.FilePath);
            });
        }
    }
}
