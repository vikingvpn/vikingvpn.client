﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Models;
using VikingVPN.Client.Services;

namespace VikingVPN.Client.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IAuthenticationService, AuthenticationService>();
            SimpleIoc.Default.Register<IConfigurationService, ConfigurationService>();
            SimpleIoc.Default.Register<IVpnService, VpnService>();
            SimpleIoc.Default.Register<ITapDriverService, TapDriverService>();
            SimpleIoc.Default.Register<AppContext>();
            SimpleIoc.Default.Register<SharedViewModel>();
            SimpleIoc.Default.Register<NotifyIconViewModel>();
            SimpleIoc.Default.Register<MainWindowViewModel>();
        }

        public MainWindowViewModel MainWindowViewModel
        {
            get { return ServiceLocator.Current.GetInstance<MainWindowViewModel>(); }
        }

        public NotifyIconViewModel NotifyIconViewModel
        {
            get { return ServiceLocator.Current.GetInstance<NotifyIconViewModel>(); }
        }
    }
}
