﻿using GalaSoft.MvvmLight.Ioc;
using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Practices.ServiceLocation;
using Squirrel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using VikingVPN.Client.Models;
using VikingVPN.Client.Services;
using VikingVPN.Client.ViewModels;
using VikingVPN.Client.Views;

namespace VikingVPN.Client
{
    public delegate void ConnectionErrorEventHandler(object sender, EventArgs e);
    public delegate void ConnectedEventHandler(object sender, EventArgs e);

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TaskbarIcon notifyIcon;

        public static event ConnectionErrorEventHandler ConnectionError;
        public static event ConnectedEventHandler Connected;

        static Mutex mutex = new Mutex(false, "e56041b2-c4d1-4672-8231-65e650afa31f");

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (!mutex.WaitOne(TimeSpan.FromSeconds(1), false))
            {
                MessageBox.Show("VikingVPN is already running!", "", MessageBoxButton.OK);
                return;
            }

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            this.Dispatcher.UnhandledException += OnDispatcherUnhandledException;
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
            ConnectionError += App_ConnectionError;
            Connected += App_Connected;
        }

        public static void TriggerConnectMessage()
        {
            if (Connected != null)
            {
                Connected(null, null);
            }
        }

        void App_Connected(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip("Connected!", "Your connection is now secure!", BalloonIcon.None);
        }

        public static void TriggerConnectionErrorPopup()
        {
            if(ConnectionError != null)
            {
                ConnectionError(null, null);
            }
        }

        void App_ConnectionError(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip("Connection lost!", "We are attempting to reconnect you now!", BalloonIcon.Error);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Cleanup();
        }

        private void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Cleanup();
        }

        void Cleanup()
        {
            try
            {
                var app = ServiceLocator.Current.GetInstance<AppContext>();
                if (app.Master.Connection != null)
                {
                    app.Master.Connection.Dispose();
                }

                notifyIcon.Dispose();
            }
            finally
            {
                mutex.ReleaseMutex();
                mutex.Dispose();
            }            
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Cleanup();
        }
    }
}
