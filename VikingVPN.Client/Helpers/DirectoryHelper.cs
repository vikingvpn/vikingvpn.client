﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Helpers
{
    public static class DirectoryHelper
    {
        public static string GetAppDataPath()
        {
            var dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VikingVPN");
            Directory.CreateDirectory(dir);
            return dir;
        }
         
        public static DirectoryInfo GetAppDataDirectory()
        {
            return new DirectoryInfo(GetAppDataPath());
        }

        public static DirectoryInfo GetApplicationDirectory()
        {
            return new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
        }

        public static string GetOpenVpnPath()
        {
            var dir = GetApplicationDirectory();
            return Path.Combine(dir.FullName, "openvpn", "libs", "openvpn.exe");
        }

        public static DirectoryInfo GetTapWindowsDirectory()
        {
            var dir = GetApplicationDirectory();
            return new DirectoryInfo(Path.Combine(dir.FullName, "openvpn", "tap-windows"));
        }

        public static string GetTapInstallPath()
        {
            var dir = GetTapWindowsDirectory();
            return Path.Combine(dir.FullName, "tapinstall.exe");
        }
    }
}
