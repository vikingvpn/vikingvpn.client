﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace VikingVPN.Client.Helpers
{
    public static class StreamExtensions
    {
        public static IEnumerable<string> ReadLines(this Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                    yield return reader.ReadLine();
            }
        }

        public static IObservable<string> ObserveLines(Stream inputStream)
        {
            return ReadLines(inputStream).ToObservable();
        }
    }
}
