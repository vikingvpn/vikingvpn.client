﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Helpers
{
    public static class WebRequestHelper
    {
        const string RootUrl = @"https://vikingvpn.com/";

        static WebRequestHelper()
        {
            //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback((a, b, c, d) => true);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }

        static Uri GetAuthenticationUri()
        {
            return new Uri(RootUrl + "account/login");
        }

        static void FillRequest(HttpWebRequest req)
        {
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";
        }

        public static HttpWebRequest GetAuthenticationRequest()
        {
            var req = WebRequest.CreateHttp(GetAuthenticationUri());
            FillRequest(req);
            return req;
        }

        static Uri GetCertGenerationUri()
        {
            return new Uri(RootUrl + "cert/generatecertificate");
        }

        public static HttpWebRequest GetCertGenerationRequest()
        {
            var req = WebRequest.CreateHttp(GetCertGenerationUri());
            FillRequest(req);
            return req;
        }

        static Uri GetIpAddressUri()
        {
            return new Uri(RootUrl + "ip");
        }

        static HttpWebRequest GetIpAddress()
        {
            var req = WebRequest.CreateHttp(GetIpAddressUri());
            FillRequest(req);
            return req;
        }

        static IPAddress ParseIp(string input)
        {
            IPAddress temp;
            if (IPAddress.TryParse(input, out temp))
                return temp;

            return null;
        }

        public static IPAddress GetCurrentIpAddress()
        {
            try
            {
                var req = GetIpAddress();
                req.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                req.KeepAlive = false;
                using (var response = req.GetResponse() as HttpWebResponse)
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var result = reader.ReadToEnd();
                    return ParseIp(result);
                }
            }
            catch (WebException) { return null; }
        }
    }
}
