﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Helpers
{
    public static class ConfigurationHelper
    {
        public static string GetCertificateDirectoryPath()
        {
            var dir = Path.Combine(DirectoryHelper.GetAppDataPath(), "config");
            Directory.CreateDirectory(dir);
            return dir;
        }

        public static DirectoryInfo GetConfigurationDirectory()
        {
            return new DirectoryInfo(GetCertificateDirectoryPath());
        }

        public const string CertificateExtension = ".ovpn";

        public static FileInfo[] GetCertificateFiles()
        {
            var dir = GetConfigurationDirectory();
            return dir.GetFiles().Where(f => string.Equals(f.Extension, CertificateExtension, StringComparison.OrdinalIgnoreCase)).ToArray();
        }

        public static string GetConfigurationZipPath()
        {
            var certDir = ConfigurationHelper.GetConfigurationDirectory();
            var certZipPath = Path.Combine(certDir.FullName, "certs.zip");
            return certZipPath;
        }
    }
}
