﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Reactive.Concurrency;

namespace VikingVPN.Client.Helpers
{
    public static class StreamReaderExtensions
    {
        public static IEnumerable<string> ReadAllLines(this StreamReader reader)
        {
            while (!reader.EndOfStream)
                yield return reader.ReadLine();
        }

        public static IObservable<string> ObserveLines(this StreamReader reader)
        {
            return reader.ReadAllLines().ToObservable(Scheduler.ThreadPool);
        }
    }
}
