﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;

namespace VikingVPN.Client.Services
{
    public class AuthToken
    {
        readonly CookieContainer _cookies;
        public AuthToken()
        {
            _cookies = new CookieContainer();
        }
        internal CookieContainer Container { get { return _cookies; } }
    }

    public interface IAuthenticationService
    {
        Tuple<string, Cookie> GetRequestVerificationTokens();
        AuthToken Authenticate(string email, string password);
    }

    public class AuthenticationService : IAuthenticationService
    {
        Tuple<string, Cookie> GetLoginPage()
        {
            var req = WebRequestHelper.GetAuthenticationRequest();
            req.CookieContainer = new CookieContainer();

            using (var response = req.GetResponse() as HttpWebResponse)
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var result = reader.ReadToEnd();
                return Tuple.Create(result, response.Cookies["__RequestVerificationToken"]);
            }
        }

        public Tuple<string, Cookie> GetRequestVerificationTokens()
        {
            var loginPage = GetLoginPage();
            var regex = new Regex(@"(<input name=""__RequestVerificationToken"" type=""hidden"" value="")(.*)("" />)");
            var match = regex.Match(loginPage.Item1);

            if(match.Success)
            {
                return Tuple.Create(match.Groups[2].Value, loginPage.Item2);
            }

            return null;
        }

        public AuthToken Authenticate(string email, string password)
        {
            var req = WebRequestHelper.GetAuthenticationRequest();
            req.CookieContainer = new CookieContainer();
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            //req.Referer = "https://vikingvpn.com/account/login";
            req.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";
            req.AllowAutoRedirect = false;

            var verificationToken = GetRequestVerificationTokens();
            req.CookieContainer.Add(verificationToken.Item2);

            var postData = string.Format("__RequestVerificationToken={0}&Email={1}&Password={2}&RememberMe=false", verificationToken.Item1, email, password);
            var data = Encoding.ASCII.GetBytes(postData);
            req.ContentLength = data.Length;

            using (var stream = req.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            try
            {
                using (var response = req.GetResponse() as HttpWebResponse)
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var result = reader.ReadToEnd();
                    var authCookie = response.Cookies[".ASPXAUTH"];
                    
                    if (authCookie != null)
                    {
                        var token = new AuthToken();
                        token.Container.Add(authCookie);
                        return token;
                    }

                    return null;
                }
            }
            catch(WebException)
            {
                return null;
            }          
        }
    }
}
