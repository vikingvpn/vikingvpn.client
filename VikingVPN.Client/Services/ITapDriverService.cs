﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;

namespace VikingVPN.Client.Services
{
    public interface ITapDriverService
    {
        string[] GetAllTapAdapters();
        bool IsTapDriverInstalled();
        void UninstallTapDriver();
        void InstallTapDriver();
        void EnsureTapDriver();
    }

    public class TapDriverService : ITapDriverService
    {
        public string[] GetAllTapAdapters()
        {
            var openvpn = DirectoryHelper.GetOpenVpnPath();
            var args = "--show-adapters";
            using (var proc = ProcessHelper.Start(openvpn, args))
            {
                var output = proc.StandardOutput.ReadAllLines().Skip(1).ToArray();
                proc.WaitForExit();
                return output;
            }
        }

        public bool IsTapDriverInstalled()
        {
            var adapters = GetAllTapAdapters();
            return adapters.Length > 0;
        }

        public void UninstallTapDriver()
        {
            var tapinstall = DirectoryHelper.GetTapInstallPath();
            var args = "remove tap0901";

            using (var proc = ProcessHelper.Start(tapinstall, args))
            {
                proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();
            }
        }

        public void InstallTapDriver()
        {
            var tapinstall = DirectoryHelper.GetTapInstallPath();
            var oemVistaPath = Path.Combine(DirectoryHelper.GetTapWindowsDirectory().FullName, "OemWin2k.inf");
            var args = string.Format("install \"{0}\" tap0901", oemVistaPath);

            using (var proc = ProcessHelper.Start(tapinstall, args))
            {
                proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();
            }
        }

        public void EnsureTapDriver()
        {
            if (IsTapDriverInstalled())
                return;

            InstallTapDriver();
        }
    }
}
