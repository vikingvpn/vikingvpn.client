﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;
using VikingVPN.Client.Models;

namespace VikingVPN.Client.Services
{
    public interface IVpnService
    {
        void KillAllInstances();
        VpnConnection Connect(string configFile);
        void Disconnect(VpnConnection conn);
    }

    public class VpnService : IVpnService
    {
        public VpnService()
        {

        }

        static VpnConnection[] GetAllCurrentInstances()
        {
            return Process.GetProcessesByName("openvpn").Select(p => new VpnConnection(p)).ToArray();
        }

        public void KillAllInstances()
        {
            foreach (var conn in GetAllCurrentInstances())
            {
                conn.Dispose();
            }
        }


        Process StartOpenVpnConnection(string configurationFilePath)
        {
            if (string.IsNullOrEmpty(configurationFilePath))
                throw new ArgumentNullException("configurationFilePath");

            if (!File.Exists(configurationFilePath))
            {
                throw new ValidationException("Invalid configuration file.");
            }

            var openvpn = DirectoryHelper.GetOpenVpnPath();

            return ProcessHelper.Start(openvpn, string.Format("--config \"{0}\"", configurationFilePath));
        }

        public VpnConnection Connect(string configurationFilePath)
        {
            var proc = StartOpenVpnConnection(configurationFilePath);
            return new VpnConnection(proc);
        }

        public void Disconnect(VpnConnection conn)
        {
            conn.Dispose();
        }
    }
}
