﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;
using VikingVPN.Client.Models;

namespace VikingVPN.Client.Services
{
    public interface IConfigurationService
    {
        IEnumerable<ConfigurationFile> GetConfigurationFiles();
        bool AreConfigurationsInstalled();
        void InstallConfigurationFiles(string email, string password, bool checkInstalled = true);
    }

    public class ConfigurationService : IConfigurationService
    {
        readonly IAuthenticationService _authService;
        public ConfigurationService(IAuthenticationService authService)
        {
            _authService = authService;
        }

        public IEnumerable<ConfigurationFile> GetConfigurationFiles()
        {
            return ConfigurationHelper.GetCertificateFiles()
            .Select(cf =>
                new ConfigurationFile
                {
                    FilePath = cf.FullName,
                    Name = Path.GetFileNameWithoutExtension(cf.FullName)
                });
        }

        public bool AreConfigurationsInstalled()
        {
            return ConfigurationHelper.GetCertificateFiles().Length > 0;
        }

        void Validate(string email, string password, bool checkInstalled)
        {
            var errors = new List<string>();
            if (string.IsNullOrEmpty(email))
            {
                errors.Add("You must supply your registered email address");
            }

            if (string.IsNullOrEmpty(password))
            {
                errors.Add("You must supply a password.");
            }

            if(checkInstalled && AreConfigurationsInstalled())
            {
                errors.Add("Your configuration files are already installed.");
            }

            if (errors.Count > 0)
                throw new ValidationException(errors);
        }

        void DownloadCertificates(AuthToken token)
        {
            if(token == null)
            {
                throw new ValidationException("Invalid username or password.");
            }

            var req = WebRequestHelper.GetCertGenerationRequest();
            req.CookieContainer = token.Container;

            var certZipPath = ConfigurationHelper.GetConfigurationZipPath();

            if (File.Exists(certZipPath))
                File.Delete(certZipPath);

            using (var response = req.GetResponse() as HttpWebResponse)
            using (var stream = response.GetResponseStream())
            using (var output = File.OpenWrite(certZipPath))
            {
                stream.CopyTo(output);
                output.Flush(true);
            }
        }

        void UnzipCertificates()
        {
            var confDir = ConfigurationHelper.GetConfigurationDirectory();
            var confZipPath = ConfigurationHelper.GetConfigurationZipPath();

            if(!File.Exists(confZipPath))
            {
                throw new ValidationException("Error retrieving certificates!");
            }

            using(var file = File.OpenRead(confZipPath))
            using(var zipFile = new ZipFile(file))
            {
                zipFile.IsStreamOwner = true;
                foreach (ZipEntry zipEntry in zipFile)
                {
                    Stream zipStream = zipFile.GetInputStream(zipEntry);

                    var certPath = Path.Combine(confDir.FullName, zipEntry.Name);

                    if (File.Exists(certPath))
                        File.Delete(certPath);

                    using(var outputFile = File.OpenWrite(certPath))
                    {
                        zipStream.CopyTo(outputFile);
                        outputFile.Flush(true);
                    }
                }
            }
        }

        public void InstallConfigurationFiles(string email, string password, bool checkInstalled = true)
        {
            Validate(email, password, checkInstalled);

            var token = _authService.Authenticate(email, password);

            DownloadCertificates(token);

            UnzipCertificates();
        }
    }
}
