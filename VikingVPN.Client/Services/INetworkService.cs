﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Services
{
    public interface INetworkService
    {
        void DisableInternetConnection();
        void EnableInternetConnection();
    }

    public class NetworkService : INetworkService
    {
        public void DisableInternetConnection()
        {
            throw new NotImplementedException();
        }

        public void EnableInternetConnection()
        {
            throw new NotImplementedException();
        }
    }
}
