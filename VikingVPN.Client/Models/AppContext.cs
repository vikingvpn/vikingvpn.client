﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Services;

namespace VikingVPN.Client.Models
{
    public class AppContext : ObservableObject
    {
        readonly Messenger _messenger = new Messenger();

        public ConnectionMaster Master { get; private set; }

        public AppContext(IConfigurationService configService, IVpnService vpnService, ITapDriverService tapService)
        {
            tapService.EnsureTapDriver();
            this.Master = new ConnectionMaster(_messenger, configService, vpnService, tapService);
        }
    }
}
