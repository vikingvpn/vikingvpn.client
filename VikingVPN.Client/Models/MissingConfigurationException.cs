﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Models
{
    public class MissingConfigurationException : Exception
    {
        public MissingConfigurationException() : base("There are no configuration files installed") { }
    }
}
