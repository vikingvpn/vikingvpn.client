﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;
using VikingVPN.Client.Services;

namespace VikingVPN.Client.Models
{
    public class ConnectionMaster : ObservableObject
    {
        readonly Messenger _messenger;
        readonly IConfigurationService _configService;
        readonly IVpnService _vpnService;
        readonly ITapDriverService _tapDriverService;

        private string _lastConnection;

        private List<ConfigurationFile> _configurations;
        public List<ConfigurationFile> Configurations
        {
            get { return _configurations; }
            set { this.Set(ref _configurations, value); }
        }

        private VpnConnection _connection;
        public VpnConnection Connection
        {
            get { return _connection; }
            set { this.Set(ref _connection, value); }
        }

        private IPAddress _originalIp;
        public IPAddress OriginalIp
        {
            get { return _originalIp; }
            set { this.Set(ref _originalIp, value); }
        }

        public ConnectionMaster(Messenger messenger, IConfigurationService configService, IVpnService vpnService, ITapDriverService tapDriverService)
        {
            _messenger = messenger;
            _configService = configService;
            _vpnService = vpnService;
            _tapDriverService = tapDriverService;

            Load();

            ThreadPool.QueueUserWorkItem((x) =>
            {
                FetchOriginalIp();
            });
        }

        public void Load()
        {
            this.Configurations = _configService.GetConfigurationFiles().ToList();
        }

        static object _lockObj = new object();

        void FetchOriginalIp()
        {
            if (this.OriginalIp != null)
                return;

            lock(_lockObj)
            {
                if (this.OriginalIp != null)
                    return;

                this.OriginalIp = WebRequestHelper.GetCurrentIpAddress();
            }
        }

        public void Connect(string configFile)
        {
            _lastConnection = configFile;
            _originalIp = null;
            _vpnService.KillAllInstances();
            FetchOriginalIp();
            
            this.Connection = _vpnService.Connect(configFile);
        }

        public void Reconnect()
        {
            if (_lastConnection == null)
                return;

            Disconnect();
            Connect(_lastConnection);
        }
        
        public void Disconnect()
        {
            if(this.Connection != null)
            {
                this.Connection.Dispose();
                this.Connection = null;
            }

            this._lastConnection = null;
        }

        public void EnsureTapAdapterInstalled()
        {
            _tapDriverService.UninstallTapDriver();
            _tapDriverService.EnsureTapDriver();
        }

        public void InstallConfigurationFiles(string email, string password)
        {
            _configService.InstallConfigurationFiles(email, password);
        }
    }
}
