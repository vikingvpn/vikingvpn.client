﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Models
{
    public class ConfigurationFile : ObservableObject
    {
        string _name;
        public string Name { get { return _name; } set { this.Set(ref _name, value); } }

        string _filePath;
        public string FilePath { get { return _filePath; } set { this.Set(ref _filePath, value); } }
    }
}
