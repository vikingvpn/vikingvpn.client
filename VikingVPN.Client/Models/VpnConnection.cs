﻿using GalaSoft.MvvmLight;
using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;

namespace VikingVPN.Client.Models
{
    public class VpnConnection : ObservableObject, IDisposable
    {
        readonly Process _openvpn;
        readonly ReactiveCollection<string> _outputCol;

        public VpnConnection(Process openvpn)
        {
            _openvpn = openvpn;
            _outputCol = new ReactiveCollection<string>();
            this.Status = ConnectionStatus.Connecting;
        }

        public IObservable<string> Output 
        { 
            get 
            {
                var output = _openvpn.StandardOutput.ObserveLines();
                output.Subscribe(x =>
                {
                    _outputCol.AddOnScheduler(x);

                    if(x != null)
                    {
                        if (x.EndsWith("Initialization Sequence Completed"))
                        {
                            this.Status = ConnectionStatus.Connected;
                        }
                        else if (x.EndsWith("Exiting due to fatal error"))
                        {
                            this.Status = ConnectionStatus.Error;
                        }
                    }
                });

                var obs = Observable
                             .FromEventPattern<NotifyCollectionChangedEventArgs>(_outputCol, "CollectionChanged")
                             .SelectMany(change => change.EventArgs.NewItems.OfType<string>());

                return obs;
            } 
        }

        public enum ConnectionStatus
        {
            Connected,
            Connecting,
            Disconnected,
            Error
        }

        ConnectionStatus _status;
        public ConnectionStatus Status
        {
            get { return _status; }
            set { this.Set(ref _status, value); }
        }

        public IObservable<string> Error { get { return _openvpn.StandardError.ObserveLines(); } }

        public void Dispose()
        {
            try
            {
                _openvpn.Kill();
            }
            catch { }
            finally 
            {
                _openvpn.Dispose();
            }
        }

        ~VpnConnection()
        {
            this.Dispose();
        }
    }
}
