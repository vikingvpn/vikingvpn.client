﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VikingVPN.Client.Models
{
    public class ValidationException : Exception
    {
        static string FormatErrors(List<string> errors)
        {
            errors = errors ?? new List<string>();
            return errors.Aggregate(new StringBuilder(), (agg, input) => agg.AppendLine(input)).ToString();
        }

        readonly List<string> _errors;
        public ValidationException(List<string> errors) : base(FormatErrors(errors)) 
        {
            _errors = errors;
        }

        public ValidationException(string error) : this(new List<string> { error }) { }

        public List<string> Errors { get { return _errors; } }
    }
}
