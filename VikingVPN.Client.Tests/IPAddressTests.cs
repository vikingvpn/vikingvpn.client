﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Helpers;

namespace VikingVPN.Client.Tests
{
    [TestClass]
    public class IPAddressTests
    {
        [TestMethod]
        public void CanGetCurrentIPAddress()
        {
            var ip = WebRequestHelper.GetCurrentIpAddress();

            Assert.IsNotNull(ip);
        }
    }
}
