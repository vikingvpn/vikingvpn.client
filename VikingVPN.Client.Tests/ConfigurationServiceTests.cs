﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VikingVPN.Client.Services;
using VikingVPN.Client.Helpers;
using VikingVPN.Client.Models;
using System.Collections.Generic;

namespace VikingVPN.Client.Tests
{
    [TestClass]
    public class ConfigurationServiceTests
    {
        IAuthenticationService GetAuthService()
        {
            return new AuthenticationService();
        }

        IConfigurationService GetService()
        {
            return new ConfigurationService(GetAuthService());
        }

        void ClearInstalledCertificates()
        {
            var certs = ConfigurationHelper.GetCertificateFiles();
            foreach(var cert in certs)
                cert.Delete();
        }

        [TestMethod]
        public void CertificatesAreNotInstalled()
        {
            ClearInstalledCertificates();

            var serv = GetService();
            Assert.IsFalse(serv.AreConfigurationsInstalled());
        }

        [TestMethod]
        public void AppDataDirectoryIsCorrectPath()
        {
            var dir = ConfigurationHelper.GetConfigurationDirectory();
            Assert.IsTrue(dir.FullName.EndsWith("Roaming\\VikingVPN\\config"));
        }

        [TestMethod]
        public void CertInstallationWithoutValidCredentialsFails()
        {
            var serv = GetService();
            List<string> errors = null;
            try
            {
                serv.InstallConfigurationFiles(null, null);
            }
            catch(ValidationException ex)
            {
                errors = ex.Errors;
            }

            Assert.AreEqual(2, errors.Count);
        }

        [Ignore]
        [TestMethod]
        public void CanInstallCertificates()
        {
            var serv = GetService();

            serv.InstallConfigurationFiles("****", "*****");

            var certs = ConfigurationHelper.GetCertificateFiles();

            Assert.IsTrue(certs.Length > 0);
        }
    }
}
