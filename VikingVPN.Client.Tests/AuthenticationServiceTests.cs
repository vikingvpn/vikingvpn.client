﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Services;

namespace VikingVPN.Client.Tests
{
    [TestClass]
    public class AuthenticationServiceTests
    {
        IAuthenticationService GetService()
        {
            return new AuthenticationService();
        }

        [TestMethod]
        public void CanGetRequestVerificationToken()
        {
            var serv = GetService();
            var res = serv.GetRequestVerificationTokens();

            Assert.IsNotNull(res);
            Assert.IsNotNull(res.Item1);
            Assert.IsNotNull(res.Item2);
            Assert.AreNotEqual(res.Item1, res.Item2);
        }

        /// <summary>
        /// Security precautions that occur on the server mean we don't want to run this test all the time.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void FailedAuthenticationResultsInNullAuthToken()
        {
            var serv = GetService();
            var token = serv.Authenticate("test@test.com", "testtest");

            Assert.IsNull(token);
        }
    }
}
