﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VikingVPN.Client.Services;

namespace VikingVPN.Client.Tests
{
    [TestClass]
    public class TapDriverServiceTests
    {
        ITapDriverService GetService()
        {
            return new TapDriverService();
        }

        [TestInitialize]
        public void Initialize()
        {
            var serv = GetService();
            serv.UninstallTapDriver();
        }

        [TestMethod]
        public void TapDriverIsNotInstalled()
        {
            var serv = GetService();
            var installed = serv.IsTapDriverInstalled();

            Assert.IsFalse(installed);
        }

        [TestMethod]
        public void CanInstallTapDriver()
        {
            var serv = GetService();
            serv.InstallTapDriver();

            var installed = serv.IsTapDriverInstalled();

            Assert.IsTrue(installed);
        }

        [TestMethod]
        public void CanRemoveTapDriver()
        {
            CanInstallTapDriver();

            var serv = GetService();
            serv.UninstallTapDriver();

            var installed = serv.IsTapDriverInstalled();

            Assert.IsFalse(installed);
        }

        [TestMethod]
        public void CanGetAllTapAdapters()
        {
            CanInstallTapDriver();

            var serv = GetService();

            var adapters = serv.GetAllTapAdapters();

            Assert.AreEqual(1, adapters.Length);
        }
    }
}
